#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 11:06:36 2017

@author: robert
"""

knotvec = [0.0, 0.0, 0.0, 1.0, 1.0, 1.0]
coords = [0.0, 7.0, 14.0]
p=2

insertknots = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0]
insertknots[:] = [v / 7.0 for v in insertknots]

for knot in insertknots:
    
    coords_copy = coords
    
    k = next(x[0] for x in enumerate(knotvec) if x[1] > knot) - 1
    
    new_coords = []
    
    alpha = 0.0
    for i in range(0,len(coords_copy) + 1):
        if i <= (k - p):
            alpha = 1.0
        elif (k - p + 1) <= i and i <= k:
            alpha = (knot - knotvec[i]) / (knotvec[i+p] - knotvec[i])
        else:
            alpha = 0.0
       
        if i < len(coords_copy):
            new_coords.append(alpha * coords_copy[i] + (1-alpha) * coords_copy[i-1])  
        else:
            new_coords.append(coords_copy[i-1])  
        
    knotvec.insert(k+1, knot)
    coords = new_coords
    
    print(knotvec)
    print(new_coords)

